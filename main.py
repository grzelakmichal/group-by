# coding=utf-8
"""
MAIN FUNCTION
"""
import os

from group_by_module import core


def main():
    """
    main
    """
    result = core.group_by(open("task/launchlog.txt"), field='year', success=None)
    print(result)
    print(sum(result.values()))


if __name__ == "__main__":
    # execute only if run as a script
    main()
