**Return grouped statistics of given file with conditions:**

**at least one date field in format YYYY MMM DD**
**at least one suc field with F or S values**
**at least two spaces sign between fields**


.. image:: http://img.shields.io/:license-mit-blue.svg
  :alt: MIT License
  :target: http://doge.mit-license.org


Supports Python 3.0+

-------

------------------------
What is Group By Module?
------------------------

Group by is module which parsing opened `stream` and calculate stats with given parameters:

field = year or month
success = None by default True or False

.. code:: python

  from group_by_module.core import group_by
  
  grouped_value = group_by(open("task/launchlog.txt"), field='year', success=None)
  print(grouped_value)