# coding=utf-8
"""
HELPERS
"""


def k_to_n_whitespaces(string, n, k):
    """
    the while loop will leave a trailing space,
    so the trailing whitespace must be dealt with
    before or after the while loop

    :param k:
    :param n:
    :param string:
    """
    replace_string = n*' '
    replaced_string = k*' '
    string = string.strip()
    while replace_string in string:
        string = string.replace(replace_string, replaced_string)
    return string


def find_parameter_field(searched_parameter, parameter_string):
    """

    :param parameter_string:
    :param searched_parameter:
    :return:
    """
    parameter_list = k_to_n_whitespaces(parameter_string, 3, 2).split('  ')

    parameters_fields = []
    for parameter in parameter_list:
        if searched_parameter in parameter.lower():
            parameters_fields.append(parameter)

    if len(parameters_fields) == 1:
        parameter_field = parameters_fields[0]
    else:
        switch = ""
        for i in range(len(parameters_fields)):
            switch += str(i) + ": " + str(parameters_fields[i]) + "\n"
        print(switch)
        while True:
            try:
                parameter_field = parameters_fields[int(input("input number: "))]
                break
            except ValueError:
                print("input is not int number")
            except IndexError:
                print("incorrect number, out of range")

    parameter_field_index = parameter_string.index(parameter_field)
    return parameter_field, parameter_field_index


def get_current_value(line, previous_value, field_index_begin, field_index_end=None, space_parameter=1):
    """

    :param line:
    :param previous_value:
    :param field_index_begin:
    :param field_index_end:
    :param space_parameter:
    :return:
    """
    current_value = line[field_index_begin:field_index_begin + 1 if field_index_end is None else field_index_end]

    if current_value == "" or current_value == space_parameter * " ":
        current_value = previous_value
    return current_value
