# coding=utf-8
"""
CORE
"""
from . import helpers

SUCCESS_DICT = {True: "S", False: "F"}


def group_by(stream, field, success=None):
    """

    :param stream:
    :param field:
    :param success:
    """

    result = {}

    headers = stream.readline()

    date_field_tuple = helpers.find_parameter_field("date", headers)
    date_field_index = date_field_tuple[1]

    success_field_tuple = helpers.find_parameter_field("suc", headers)
    success_field_index = success_field_tuple[1]

    stream.readline()

    if field == "year":
        parameter_index_begin = date_field_index
        space_parameter = 4
        parameter_index_end = parameter_index_begin + space_parameter

    elif field == "month":
        parameter_index_begin = date_field_index + 5
        space_parameter = 3
        parameter_index_end = parameter_index_begin + space_parameter

    else:
        raise ValueError("Incorrect values, acceptable year or month")

    previous_success = None
    previous_date = None

    for line in stream:

        current_success = helpers.get_current_value(line, previous_success, success_field_index)
        current_date = helpers.get_current_value(line, previous_date, parameter_index_begin, parameter_index_end,
                                                 space_parameter)

        if success is None or SUCCESS_DICT[success] == current_success:
            try:
                result[current_date] += 1
            except KeyError:
                result[current_date] = 1

        previous_success = current_success
        previous_date = current_date
    return result
