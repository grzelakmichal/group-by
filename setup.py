# -*- coding: utf-8 -*-
"""
SETUP
"""

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    lic = f.read()

setup(
    name='group-by',
    version='0.1.0',
    description='Group by package',
    long_description=readme,
    author='Michal Grzelak',
    author_email='grzelak.michael@gmail.com',
    url='https://bitbucket.org/grzelakmichal/group-by',
    license=lic,
    packages=find_packages(exclude=('tests', 'docs'))
)
