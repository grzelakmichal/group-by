# coding=utf-8
"""
BASIC TESTS
"""
import os
import unittest
import random
from unittest.mock import patch

from unittest import TestCase

from tests.context import group_by_module


class TestKToNSpaces(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestKToNSpaces, cls).setUpClass()
        cls.k = random.randint(0, 10 ** 6)
        cls.n = random.randint(0, cls.k)

    def test_same_whitespaces(self):
        for i in range(10):
            self.assertEqual(group_by_module.helpers.k_to_n_whitespaces(i * ' \n\t', self.k, self.n), '')

    def test_end_whitespaces(self):
        self.assertEqual(group_by_module.helpers.k_to_n_whitespaces('end whitespace   \n',
                                                                    self.k, self.n), 'end whitespace')

    def test_start_whitespaces(self):
        self.assertEqual(group_by_module.helpers.k_to_n_whitespaces('\n   start whitespace',
                                                                    self.k, self.n), 'start whitespace')

    def test_random_headers(self):
        random_header = "column_1" + self.k * ' ' + "column_2"
        result_header = "column_1" + self.n * ' ' + "column_2"
        self.assertEqual(group_by_module.helpers.k_to_n_whitespaces(random_header, self.k, self.n), result_header)

    def test_example_headers(self):
        example_header = "# Launch     Launch Date (UTC)    COSPAR               PL Name                        Orig " \
                         "PL Name              SATCAT   LV Type                LV S/N          Site                  " \
                         "           Suc  Ref"
        result_header = "# Launch  Launch Date (UTC)  COSPAR  PL Name  Orig PL Name  SATCAT  LV Type  LV S/N  Site  " \
                        "Suc  Ref"

        self.assertEqual(group_by_module.helpers.k_to_n_whitespaces(example_header, 3, 2), result_header)


class TestFindParameterField(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestFindParameterField, cls).setUpClass()
        cls.parameter_string = "# Launch     Launch Date (UTC)    COSPAR"
        cls.searched_parameter = "date"

    def test_more_than_one(self):
        parameter_string = "# Launch     Launch Date (UTC)    COSPAR  date"
        searched_parameter = "date"

        with patch('builtins.input', side_effect=[0]):
            a = group_by_module.find_parameter_field(searched_parameter, parameter_string)

        self.assertEqual(a, ("Launch Date (UTC)", 13))

    def test_find_parameter_field(self):
        self.assertEqual(group_by_module.find_parameter_field(self.searched_parameter, self.parameter_string),
                         ("Launch Date (UTC)", 13))


if __name__ == '__main__':
    unittest.main()
